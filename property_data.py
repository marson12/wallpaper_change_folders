class PropertyData:
    """
    This is the data object for this program.
    it is passed around the classes.
    it is then made into a property file in property_controller
    when the program is turned on again, either by person or from .bat
    then all these settings will be loaded again.
    """

    def __init__(self):
        # settings variables
        self.notify_on_wallpaper = True
        self.change_speed = 60
        self.random_style = 'random shuffle'

        # server
        self.run_engine = True
        self.port = 8097

        # scan variables
        self.top_directory = ''

        self.black_list_regex = []
        self.black_list_contains = []
        self.black_list_path = []

        self.white_list_regex = []
        self.white_list_contains = []
        self.white_list_path = []
        self.whitelist_subfolders = False

        # errors
        self.error_list = []

    # settings variables
    def set_notifier_boolean(self, notify_on_wallpaper):
        self.notify_on_wallpaper = notify_on_wallpaper

    def get_notifier_boolean(self):
        return self.notify_on_wallpaper

    def set_change_speed(self, change_speed):
        self.change_speed = change_speed

    # /TODO change this to server
    #     TODO
    #     TODO
    def get_change_speed(self):
        return self.change_speed

    def set_random_style(self, style):
        self.random_style = style

    def get_random_style(self):
        return self.random_style

    # server

    def set_is_run_engine(self, is_run):
        self.run_engine = is_run

    def is_run_engine(self):
        return self.run_engine

    def set_port(self, port):
        self.port = port

    def get_port(self):
        return self.port

    # scan variables
    def set_top_directory(self, directory):
        self.top_directory = directory

    def get_top_directory(self):
        return self.top_directory

    def set_black_list_regex(self, black_list_regex):
        self.black_list_regex = black_list_regex

    def get_black_list_regex(self):
        return self.black_list_regex

    def set_black_list_contains(self, black_list_contains):
        self.black_list_contains = black_list_contains

    def get_black_list_contains(self):
        return self.black_list_contains

    def set_black_list_path(self, black_list_path):
        self.black_list_path = black_list_path

    def get_black_list_path(self):
        return self.black_list_path

    def set_white_list_regex(self, white_list_regex):
        if white_list_regex == ['']:
            self.white_list_regex = []
        else:
            self.white_list_regex = white_list_regex

    def get_white_list_regex(self):
        return self.white_list_regex

    def set_white_list_contains(self, white_list_contains):
        if white_list_contains == ['']:
            self.white_list_contains = []
        else:
            self.white_list_contains = white_list_contains

    def get_white_list_contains(self):
        return self.white_list_contains

    def set_white_list_path(self, white_list_path):
        if white_list_path == ['']:
            self.white_list_path = []
        else:
            self.white_list_path = white_list_path

    def get_white_list_path(self):
        return self.white_list_path

    def set_whitelist_subfolder(self, is_include_subfolders):
        if is_include_subfolders == ['']:
            self.whitelist_subfolders = []
        else:
            self.whitelist_subfolders = is_include_subfolders

    def get_whitelist_subfolder(self):
        return self.whitelist_subfolders

    def set_error_list(self, error_list):
        self.error_list = error_list

    def get_error_list(self):
        return self.error_list

    def add_error(self, error):
        self.error_list.append(str(error))

