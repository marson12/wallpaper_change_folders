import random
import configparser
import property_data
import os_util
import win10toast
from property_data import PropertyData
import re
import util


class PropertyController:
    """
    this class is used to handle the 3 property files.

    properties_wallpaper.ini: holds all settings data.
        when this class is loaded it will open that file
        then it will load all the information of that file
        and send it out to change the settings. like installer_wallpaper

    directories_list.txt: this is the list of all image_paths that were scanned.
        how the scan is executed is based on the settings of the properties object
        the images are then saved to this list.
        then returned whenever the main file asks.

    image_log.txt: this is a log of recently seen images.
        whenever a new image is grabbed its path will be put onto the TOP of the list.
        when the list has 50 or more lines then it will drop the last image.
    """

    def __init__(self, data=None):
        self.config = configparser.ConfigParser()

        dir = os_util.get_working_directory()
        self.property_file_destination = dir + '_properties_wallpapers.ini'
        self.directories_list = dir + '_directories_list.txt'
        self.image_log = dir + '_image_log.txt'

        if data:
            self.data = property_data
        else:
            self.data = PropertyData()
            self.find_properties()
        #         the file has not been created yet. pass.

    def find_properties(self):
        if os_util.file_exists(self.property_file_destination):
            self.config.read(self.property_file_destination)

            # settings for when running
            self.data.set_notifier_boolean(self.config.getboolean('settings', 'notify_on_wallpaper'))
            self.data.set_change_speed(self.config.getfloat('settings', 'change_speed'))
            self.data.set_random_style(self.config.get('settings', 'random_style')),

            # server settings
            self.data.set_port(int(self.config.get('server', 'port')))

            # scanning settings
            self.data.set_top_directory(self.config.get('scanning', 'top_directory'))

            self.data.set_black_list_regex(
                self.delimiter_string_to_list(self.config.get('scanning', 'black_list_regex')))
            self.data.set_black_list_contains(
                self.delimiter_string_to_list(self.config.get('scanning', 'black_list_contains')))
            self.data.set_black_list_path(
                self.delimiter_string_to_list(self.config.get('scanning', 'black_list_path')))

            self.data.set_white_list_regex(
                self.delimiter_string_to_list(self.config.get('scanning', 'white_list_regex')))
            self.data.set_white_list_contains(
                self.delimiter_string_to_list(self.config.get('scanning', 'white_list_contains')))
            self.data.set_white_list_path(
                self.delimiter_string_to_list(self.config.get('scanning', 'white_list_path')))

            self.data.set_whitelist_subfolder(self.config.get('scanning', 'whitelist_subfolders'))

            # errors list
            self.data.set_error_list(self.delimiter_string_to_list(self.config.get('errors', 'error_list')))
        else:
            self.data.set_top_directory(os_util.get_pictures_directory())

    def set_property_file(self):
        self.config['settings'] = {
            'notify_on_wallpaper': str(self.data.get_notifier_boolean()),
            'change_speed': str(self.data.get_change_speed()),
            'random_style': self.data.get_random_style()
        }
        self.config['server'] = {
            'port': self.data.get_port()
        }

        self.config['scanning'] = {
            'top_directory': self.data.get_top_directory(),

            'black_list_regex': self.list_to_delimiter_string(self.data.get_black_list_regex()),
            'black_list_contains': self.list_to_delimiter_string(self.data.get_black_list_contains()),
            'black_list_path': self.list_to_delimiter_string(self.data.get_black_list_path()),

            'white_list_regex': self.list_to_delimiter_string(self.data.get_white_list_regex()),
            'white_list_contains': self.list_to_delimiter_string(self.data.get_white_list_contains()),
            'white_list_path': self.list_to_delimiter_string(self.data.get_white_list_path()),

            'whitelist_subfolders': self.data.get_whitelist_subfolder()
        }

        self.config['errors'] = {
            'error_list': self.list_to_delimiter_string(self.data.get_error_list())
        }

        with open(self.property_file_destination, 'w') as file:
            self.config.write(file)

    def install(self):
        os_util.add_to_startup()
        os_util.delete_file(self.directories_list)
        self.set_property_file()
        os_util.add_rightclick_options(self.data.get_port())

    def update(self):
        os_util.delete_file(self.directories_list)
        self.set_property_file()

    def uninstall(self):
        error = ''
        os_util.remove_from_startup()
        os_util.delete_file(self.property_file_destination)
        os_util.delete_file(self.directories_list)
        os_util.delete_file(self.image_log)
        new_error = os_util.remove_rightclick_options()
        if new_error:
            error += new_error
        return error

    def __next__(self):
        return self.get_image()

    def __iter__(self):
        return self

    def get_image(self):
        image_path = ''
        if self.data.get_random_style() == 'random shuffle':
            image_path = self.return_remove_line()
            if image_path == '':
                top = self.data.get_top_directory()
                files = self.get_image_list(top, True)
                random.shuffle(files)
                self.write_lines(files)
                image_path = self.return_remove_line()

        elif self.data.get_random_style() == 'order':
            image_path = self.return_remove_line()
            if image_path == '':
                files = self.get_image_list(self.data.get_top_directory(), True)
                files.reverse()
                self.write_lines(files)
                image_path = self.return_remove_line()

        elif self.data.get_random_style() == 'random':
            if hasattr(self, 'has_scanned'):
                return self.return_line()
            else:
                files = self.get_image_list(self.data.get_top_directory(), True)
                random.shuffle(files)
                self.write_lines(files)
                image_path = self.return_line()
                self.has_scanned = True
                return image_path

        if self.data.get_notifier_boolean():
            image_name = image_path.split('\\')[-1]
            os_util.notify('folder: ' + image_path.replace('\\' + image_name, ''), 'image: ' + image_name)

        self.image_log_add(image_path)
        return image_path

    def previous_image_to_next(self):
        # get the 2 most recent log files
        lines = []
        with open(self.image_log, 'r') as read_file:
            log_lines = read_file.readlines()
            lines.append(log_lines.pop(1))
            lines.append(log_lines.pop(1))

        with open(self.image_log, 'w+') as write_file:
            write_file.writelines(log_lines)

        with open(self.directories_list, 'r') as read_file:
            directory_lines = read_file.readlines()
        directory_lines.append(lines.pop(0))
        directory_lines.append(lines.pop())

        self.write_lines(directory_lines)

    def favorite_current_image(self):
        image_path = ''
        with open(self.image_log, 'r') as read_file:
            log_lines = read_file.readlines()
            image_path = log_lines.pop(1)
            image_path = image_path.rstrip(
                image_path[-1])  # remove the new line character that is at the end of the file

        destination = self.data.get_top_directory() + '/favorites_wallpaper_playlist'
        os_util.get_make_folder(destination)
        os_util.copy_file(image_path, destination)

    def delete_current_image(self):
        image_path = ''

        # get the lines from the log.
        # remove the most recent line from the log
        # set the image_path as that line
        with open(self.image_log, 'r') as read_file:
            log_lines = read_file.readlines()
            image_path = log_lines.pop(1)
            image_path = image_path.rstrip(image_path[-1])  # the last character is a new line, must be removed

        # move the file to the delete directory
        destination = self.data.get_top_directory() + '/delete_wallpaper_playlist'
        os_util.get_make_folder(destination)
        os_util.copy_file(image_path, destination)
        os_util.delete_file(image_path)

        # rewrite the file with the log lines
        # that had the current wallpaper removed
        with open(self.image_log, 'w+') as write_file:
            write_file.writelines(log_lines)

    # depth first search
    def get_image_list(self, folder_path, is_grab):
        """
        this creates the list of images.
        it recursivly calls this method to depth first
        go through the file system, starting at top_directory.
        there are 3 modes that the method could be in.
        STOP: the folder didnt validate and wont be scanned recursivly
        COMTINUE: there are whitelist items, and this file isnt included
            it will continue to scan but it wont get any new files
        GRAB: it will grab all the files in that folder
        """
        directory_items = os_util.get_directory_items(folder_path)

        # the image files
        files = []

        # the folders found in the directory of folder_path
        folders = []

        for entry in directory_items:
            full_path = os_util.join_path(folder_path, entry)

            if os_util.is_folder(full_path):
                folders.append(full_path)
            else:
                if is_grab and self.validate_file(full_path):
                    files.append(full_path)

        for folder in folders:
            evaluation = self.whitelist_blacklist_evaluation(folder)
            if evaluation == 'STOP':
                pass

            elif evaluation == 'CONTINUE':
                if is_grab and self.data.get_whitelist_subfolder() \
                        and folder_path != self.data.get_top_directory():
                    files.extend(self.get_image_list(folder, True))
                else:
                    files.extend(self.get_image_list(folder, False))

            elif evaluation == 'GRAB':
                files.extend(self.get_image_list(folder, True))
        return files

    def whitelist_blacklist_evaluation(self, directory):
        """
        :param directory: must be a valid directory path
        :return:
            STOP: the directory given is matched with a blacklist item.
            CONTINUE: whitelist contains items, and this directory is not included
            GRAB: whitelist is empty, or the directory matches with a whitelist item.
        """
        if 'delete_wallpaper_playlist' in directory:
            print('mason')

        for reg in self.data.get_black_list_regex():
            if reg and re.match(reg, directory):
                return 'STOP'

        for contain in self.data.get_black_list_contains():
            if contain and contain in directory:
                return 'STOP'

        for path in self.data.get_black_list_path():
            if os_util.is_same_file(path, directory):
                return 'STOP'

        for reg in self.data.get_white_list_regex():
            if reg and re.match(reg, directory):
                return 'GRAB'

        for contain in self.data.get_white_list_contains():
            if contain and contain in directory:
                return 'GRAB'

        for path in self.data.get_white_list_path():
            if path == directory:
                return 'GRAB'

        white_list_size = len(self.data.get_white_list_regex()) + len(
            self.data.get_white_list_contains()) + len(self.data.get_white_list_path())
        if white_list_size:
            return 'CONTINUE'
        else:
            return 'GRAB'

    def validate_file(self, file):
        # check for file type
        extension = file.split(".")[-1]

        if extension.strip().lower() in util.imageFileTypes():
            return True
        # bad = ['gif', 'ini', 'db', 'json', 'lnk', 'txt', 'ico', 'mp4', 'wav', 'url', 'mp3']
        # if extension.strip().lower() in bad:
        #     return False
        return False

    def write_lines(self, line_array):
        os_util.delete_file(self.directories_list)
        with open(self.directories_list, 'w+', encoding='utf-8') as file:
            for line in line_array:
                file.write(line + '\n')

    def return_remove_line(self):
        if not os_util.file_exists(self.directories_list):
            return ''

        with open(self.directories_list, 'r') as read_file:
            lines = read_file.readlines()

        if len(lines) == 0:
            return ''

        with open(self.directories_list, 'w') as write_file:
            first_line = lines.pop()
            write_file.writelines(lines)

        image_path = first_line.strip()
        if not os_util.file_exists(image_path):
            return self.return_remove_line()
        return first_line.strip()

    def return_line(self):
        with open(self.directories_list, 'r') as read_file:
            lines = read_file.readlines()
            choice = random.randint(0, len(lines))
            first_line = lines[choice]

        image_path = first_line.strip()
        if not os_util.file_exists(image_path):
            return self.return_line()
        return image_path

    def image_log_add(self, image_path):
        if not (os_util.file_exists(self.image_log)):
            with open(self.image_log, 'w') as write_file:
                into_msg = '        NEWEST AT TOP\n'
                write_file.writelines(into_msg)

        with open(self.image_log, 'r') as read_file:
            old_lines = read_file.readlines()

        # the first line is the message at the top
        if len(old_lines) > 50:
            old_lines.pop()

        with open(self.image_log, 'w+') as write_file:
            old_lines.insert(1, image_path + '\n')
            write_file.writelines(old_lines)

    def list_to_delimiter_string(self, string_list):
        delimiter = os_util.get_invalid_folder_symbol()
        string = ''
        for elm in string_list:
            if string == '':
                string = elm
            else:
                string += delimiter + elm
        return string

    def delimiter_string_to_list(self, string):
        delimiter = os_util.get_invalid_folder_symbol()
        return string.split(delimiter)

    def get_data(self):
        return self.data

    def add_error(self, str):
        self.data.add_error(str)
        self.set_property_file()
