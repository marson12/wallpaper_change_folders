import ctypes
import getpass
import os
from win10toast import ToastNotifier
import sys
import shutil
from win32event import CreateMutex
from win32api import GetLastError
from winerror import ERROR_ALREADY_EXISTS
import winreg


def get_os():
    return sys.platform


def get_invalid_folder_symbol():
    if get_os() == 'win32':
        return '?'
    elif get_os() == 'linux':
        return '\0'
    # macOS will accept pretty much everything. So the plan is just make a lot of unlikely characters in a row
    elif get_os() == 'darwin':
        return 'this_wallpaper_changer_thing_is_lame'


def set_wallpaper(image):
    if get_os() == 'win32':
        ctypes.windll.user32.SystemParametersInfoW(20, 0, image, 0)


def notify(header, message):
    if get_os() == 'win32':
        pass
        # ToastNotifier().show_toast(header, message)
        # TODO fix the messasnger


def add_to_startup():
    if get_os() == 'win32':
        file_path = os.path.dirname(sys.argv[0])
        file_path = file_path[:len(file_path)] + '/wallpaper_playlist'
        USER_NAME = getpass.getuser()
        bat_path = r'C:\Users\%s\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup' % USER_NAME
        with open(bat_path + '\\wallpaper_playlist.bat', "w+") as bat_file:
            bat_file.write(r'start "" "' + file_path + '.exe" "automatic"')


def remove_from_startup():
    if get_os() == 'win32':
        USER_NAME = getpass.getuser()
        bat_path = r'C:\Users\%s\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup' % USER_NAME
        path = bat_path + '\\main_wallpaper_playlist.bat'
        delete_file(path)


def add_rightclick_options(port_number):
    port = str(port_number)

    command_list = [['Options', 'installer'],
                    ['Continue', 'start'],
                    ['Pause', 'stop'],
                    ['Back', 'previous'],
                    ['Skip', 'skip'],
                    ['Favorite', 'favorite'],
                    ['Delete', 'delete']
                    ]

    for title, command in command_list:
        add_rightclick_option(title, command, port)

    # add_rightclick_option('Options', 'installer', port)
    # add_rightclick_option('Continue', 'start', port)
    # add_rightclick_option('Pause', 'stop', port)
    # add_rightclick_option('Back', 'previous', port)
    # add_rightclick_option('Skip', 'skip', port)
    # add_rightclick_option('Favorite', 'favorite', port)
    # add_rightclick_option('Delete', 'delete', port)


def add_rightclick_option(title, command, port):
    '''

    :param title: the text that will appear in the right click menu
    :param command: the request that will be sent when that right click is called
    :param port: the port that the request will be sent to
    :return:
    '''

    if get_os() == 'win32':
        try:
            file_path = os.path.dirname(sys.argv[0])
            file_path = file_path[:len(file_path)] + '\\Wallpaper_playlist.exe'

            key_path = r'Directory\Background\shell\\Wallpaper ' + title
            key = winreg.CreateKeyEx(winreg.HKEY_CLASSES_ROOT, key_path)
            winreg.SetValue(key, '', winreg.REG_SZ, title)

            key1 = winreg.CreateKeyEx(key, r'command')
            winreg.SetValue(key1, '', winreg.REG_SZ,
                            'curl --request PATCH http:/localhost:' + port + '/' + command)
            # '"' + file_path + '" "' + command + '"')
        except(PermissionError):
            # user did not launch with admin
            pass


def remove_rightclick_options():
    titles = ['Skip', 'Continue', 'Back', 'Options', 'Pause', 'Favorite', 'Delete']
    isFailRemove = False
    for title in titles:
        try:
            file_path = r'Directory\Background\shell\Wallpaper ' + title + r'\command'
            open_key = winreg.OpenKey(winreg.HKEY_CLASSES_ROOT, file_path)
            winreg.DeleteKey(open_key, '')

            file_path = r'Directory\Background\shell\Wallpaper ' + title
            open_key = winreg.OpenKey(winreg.HKEY_CLASSES_ROOT, file_path)
            winreg.DeleteKey(open_key, '')
        except(Exception):
            # user did not launch with admin
            isFailRemove = 'failed to remove. restart in admin mode, otherwise the right click options will not be removed. '
            pass
    return isFailRemove


def delete_file(path):
    if file_exists(path):
        os.unlink(path)


def file_exists(path):
    return os.path.exists(path)


def get_directory_items(folder_path):
    return os.listdir(folder_path)


def join_path(part1, part2):
    return os.path.join(part1, part2)


def is_folder(folder):
    return os.path.isdir(folder)


def make_folder(path):
    os.mkdir(path)


def get_make_folder(path):
    '''
    if the directory does not exist, this will create that folder
    if the directory does exist then this will do nothing
    this is to remove the need to check if a directory already exists when
        a directory is being made
    :param path:
    :return:
    '''
    if not file_exists(path):
        os.mkdir(path)


def get_sys_args():
    return sys.argv


def get_working_directory():
    return sys.argv[0]


def move_file_folder(source, destination):
    shutil.move(source, destination)


def copy_file(source, destination):
    shutil.copy(source, destination)


def print_sysargs():
    print(sys.argv)


def get_pictures_directory():
    home = os.path.expanduser('~')
    image = os.path.join(home, 'Pictures')
    return image


def is_duplicate_instance():
    if get_os() == 'win32':
        mutex = CreateMutex(None, 1, "wallpaper playlist mutex value")
        is_duplicate = False
        if GetLastError() == ERROR_ALREADY_EXISTS:
            is_duplicate = True
        return is_duplicate, mutex

def is_same_file(patch1, path2):
    return os.path.samefile(patch1, path2)