import os_util
from property_controller import PropertyController
import property_data
from installer_wallpaper import Installer
from server.server import WallpeperServer
from server import access_endpoints

import multiprocessing


def get_instance_lock(manager):
    isDuplicate, mutex = os_util.is_duplicate_instance()
    if isDuplicate:
        data = manager.get_data()
        access_endpoints.send_open_installer(data.get_port())
        exit(1)
    else:
        # this ensures that there is only one instance of this program.
        # must keep the mutex in memory
        return mutex


def handle_engine(data, manager):
    while True: #try to continue even if there is an error, dont want the hassle of stopping.
        try:
            if data.is_run_engine():
                server = WallpeperServer(data, manager)
                server.serve_forever()
        except Exception as e:
            manager.add_error(str(e.with_traceback()))


if __name__ == '__main__':
    """
    Main method. this is ran 2 different ways. 
    1) if the user clicks this executable and it opens a gui installer_wallpaper. after running the install this
            program will begin changing wallpapers according to the options given to the gui. 
    2) this file is opened by wallpaper_playlist.bat in the folder 
    'C:\\users\%s\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup' % USER_NAME
    The program will begin flipping through the images as specified"""

    # needed for multiprocessing to be included in a windows executable.
    # multiprocessing found in server.server.WallpeperServer
    multiprocessing.freeze_support()

    manager = PropertyController()
    args = os_util.get_sys_args()
    instance_lock = None
    data = manager.get_data()

    if len(args) > 1:
        instruction = args[1]

        print(instruction)

        if instruction == 'automatic':  # started by windows startup function
            instance_lock = get_instance_lock(manager)
            handle_engine(data, manager)

        else:
            if instruction == 'installer':
                access_endpoints.send_open_installer(data.get_port())
            elif instruction == 'start':
                access_endpoints.engine_start(data.get_port())
            elif instruction == 'stop':
                access_endpoints.engine_stop(data.get_port())
            elif instruction == 'previous':
                access_endpoints.set_previous_image(data.get_port())
            elif instruction == 'skip':
                access_endpoints.skip_image(data.get_port())

    else:
        # this is if the program is opened by a user
        lock = get_instance_lock(manager)
        Installer(manager)  # if user decides to exit out of installer then the engine will not start.
        handle_engine(data, manager)

    # C:\Users\{username}\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup
    # pyinstaller --onefile --icon=recources\icon.ico wallpaper_playlist.py -w
    #  if the "-w" is removed then there will be a console associated with the program
    #  and you can see print statements from that in the exe mode.
    # NAME wallpaper playlist
    # message that the folders the exe is in MUST NOT HAVE SPACES IN THEIR NAMES.
    # TODO somehow scan based on the picture resolution, like only big 1920x1080, no 1920x540
    # TODO scan based on the colors of the image. ex: images that are mostly black
    #   and a secondary color, possibly 3

#     TODO add an icon for both this app and the right click menu options
