from PIL import Image
import util
import shutil
import os_util


def convert_folder(folder_path):
    paths = get_image_list(folder_path)
    for path in paths:
        convert(path)


def get_image_list(path):
    # the image files
    file_paths = []

    # the folders found in the directory of folder_path
    folders = []

    directory_items = os_util.get_directory_items(path)

    for entry in directory_items:
        full_path = os_util.join_path(path, entry)

        if os_util.is_folder(full_path):
            folders.append(full_path)
        else:
            file_paths.append(full_path)

    for folder in folders:
        file_paths.extend(get_image_list(folder))

    return file_paths


def convert(path):
    acceptableFileTypes = util.imageFileTypes()
    if path.split(".")[-1].lower() in acceptableFileTypes + ['gif', 'mp4']:
        return
    try:
        im = Image.open(path).convert('RGB')
        path_extensionless = path.split(".")[-2]
        path_extensionless = path_extensionless.split('\\')[-1]
        path_base_arr = path.split('\\')
        path_base_arr.pop()
        path_base = ''
        for directory in path_base_arr:
            path_base += directory + '\\'

        converted_path = path_base + 'CONVERTED\\'
        if not os_util.file_exists(converted_path):
            os_util.make_folder(converted_path)

        converted_path += path_extensionless
        im.save(converted_path + '.png')

        bad_path = path_base + 'BAD_FILE_TYPE\\'
        if not os_util.file_exists(bad_path):
            os_util.make_folder(bad_path)

        shutil.move(path, bad_path)
    except:
        # the time where it usually fails is when the thing being converted is not an image file,
        # like an excel doc, or a word doc, or something like that
        print(path)


def get_file_types(file_paths):
    file_types = []
    bad_files = []

    for path in file_paths:
        type = path.split(".")[-1].lower()
        if type not in file_types:
            file_types.append(type)

        if type not in util.imageFileTypes():
            bad_files.append(path)

    print(file_types)
    print(bad_files)
