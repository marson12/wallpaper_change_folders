import tkinter
from tkinter import Tk, Frame, Button, Label, Entry, IntVar, Checkbutton, filedialog, Toplevel, \
    StringVar, END
import time
import os_util
from property_data import PropertyData

"""
This is a group of classes that are used in installer_wallpaper. 
"""


class CreateToolTip(object):
    '''
    create a tooltip for a given widget
    '''

    def __init__(self, widget, text='widget info'):
        self.widget = widget
        self.text = text
        self.widget.bind("<Enter>", self.enter)
        self.widget.bind("<Leave>", self.close)

    def enter(self, event=None):
        time.sleep(1)
        x = y = 0
        x, y, cx, cy = self.widget.bbox("insert")
        x += self.widget.winfo_rootx() + 25
        y += self.widget.winfo_rooty() + 20
        # creates a toplevel window
        self.tw = Toplevel(self.widget)
        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = Label(self.tw, text=self.text, justify='left',
                      background='lightblue', relief='solid', borderwidth=1,
                      font=("times", "8", "normal"))
        label.pack(ipadx=1)

    def close(self, event=None):
        if self.tw:
            self.tw.destroy()

    def place(self, x, y):
        self.widget.place(x=x, y=y)


class NumEntry(Entry):
    """
    this is an entry that only can contain numbers, or blank spaces.
    """

    def __init__(self, master=None, **kwargs):
        self.var = StringVar()
        Entry.__init__(self, master, textvariable=self.var, **kwargs)
        self.old_value = ''
        self.var.trace('w', self.check)
        self.get, self.set = self.var.get, self.var.set

    def check(self, *args):
        if self.is_float():
            # the current value is only digits; allow this
            self.old_value = self.get()
        else:
            # there's non-digit characters in the input; reject this
            self.set(self.old_value)

    def is_float(self):
        num = self.get()
        if not num:
            return True
        try:
            float(num)
            return True
        except ValueError:
            return False

    def set_num(self, num):
        self.set(num)

    def get_num(self):
        return self.get()


class ListGui(Frame):
    """
    this gui has 2 modes. blacklist, and whitelist.
    allows the user to fill out 3 lists. give the information to a data object
    and then close itself.
    as entrys are filled out there are more entries created.
    the defaults are from the data object.
    """

    def __init__(self, data, is_black_list):
        self.master = Toplevel()
        Frame.__init__(self, self.master)
        self.master.geometry('1300x500')

        if is_black_list:
            self.master.title('Blacklist Settings')
        else:
            self.master.title('Whitelist Settings')

        self.data = data
        self.is_black = is_black_list

        self.regex_list = []
        self.contains_list = []
        self.exact_path_list = []

        self.regex_x = 35
        self.contains_x = 400
        self.path_x = 800
        self.error_lbl = Label(self.master, text='')

        # into
        if self.is_black:
            intro_msg = '''This is used to filter when the program is scanning your files.
                        If a FOLDER is listed by any of the 3 blacklist types then when scanning 
                        it will not be scanned. No images from it will be taken. If a folder is whitelisted and 
                        blacklisted then it will still skipped.'''
        else:
            intro_msg = '''This is used to filter when the program is scanning your files. 
                        If ANY of these fields are filled then the only images taken will be from folders that are 
                        listed by any of the 3 whitelists. If all these fields are empty then all folders
                        under the "top directory:" will have their images scanned. Unless a folder is blacklisted. 
                        '''
        intro = Label(self.master, text=intro_msg)
        intro.place(x=self.regex_x, y=15)

        # finish button
        add_list_button = Button(self.master, text='FINISH', command=self.add_list)

        add_list_button.place(x=self.path_x, y=50)

        if not self.is_black:
            self.is_whitelist_subfolders = IntVar()

            self.whitelist_subfolders_check = Checkbutton(self.master,
                                                          text='If a folder is whitelisted, its subfolders are as well.',
                                                          variable=self.is_whitelist_subfolders)
            self.whitelist_subfolders_check.place(x=self.path_x + 75, y=50)

        # lists
        list_lbl_height = 110

        self.regex_lbl = Label(self.master, text='Use regular expressions to list files')
        self.regex_lbl = CreateToolTip(self.regex_lbl, '''If you dont understand regular expressions then dont worry 
        about this. Just use the other ones if you want. If a folder matches a regex expression then it will be 
        whitelisted.''')

        self.regex_lbl.place(x=self.regex_x, y=list_lbl_height)
        self.error_lbl.place(x=self.contains_x, y=0)

        self.contain_lbl = Label(self.master, text='If the folder NAME contains these word(s) then it will be listed.')
        self.contain_lbl = CreateToolTip(self.contain_lbl, '''Example: textboxfield = steam
             any folder that contains the words steam in it will be listed.
             Both folders "steam" and "steam designs" will be listed, but not the folders "steeam", and "STEAM". ''')
        self.contain_lbl.place(x=self.contains_x, y=list_lbl_height)

        self.exact_path_lbl = Label(self.master, text='Folders with this path will be listed.')
        self.exact_path_lbl = CreateToolTip(self.exact_path_lbl, 'Yea. Its. Its that. Paths in this are listed.')
        self.exact_path_lbl.place(x=self.path_x, y=list_lbl_height)

        self.add_member('regex')
        self.add_member('contains')
        self.add_member('exact_path')

        self.defaults()

        # self.master.mainloop()

    def add_member(self, list_type, text=None):
        start_height = 100

        # add the member to the list
        member_entry = Entry(self.master, width=40)
        if list_type == 'regex':
            if len(self.regex_list) > 1 and not self.regex_list[-2].get():
                return

            if text:
                self.regex_list[-1].insert(0, text)

            member_entry.bind('<Button-1>', lambda x: self.add_member('regex'))
            self.regex_list.append(member_entry)
            member_entry.place(x=self.regex_x, y=len(self.regex_list) * 35 + start_height)

        elif list_type == 'contains':
            if len(self.contains_list) > 1 and not self.contains_list[-2].get():
                return

            if text:
                self.contains_list[-1].insert(0, text)

            member_entry.bind('<Button-1>', lambda x: self.add_member('contains'))
            self.contains_list.append(member_entry)

            member_entry.place(x=self.contains_x, y=len(self.contains_list) * 35 + start_height)

        elif list_type == 'exact_path':
            if len(self.exact_path_list) > 1 and not self.exact_path_list[-2][0].get():
                return

            if text:
                self.exact_path_list[-1][0].insert(0, text)

            member_entry.bind('<Button-1>', lambda x: self.add_member('exact_path'))

            def set_directory():
                path = filedialog.askdirectory()
                if not path:
                    return
                member_entry.delete(0, END)
                member_entry.insert(0, path)
                self.add_member('exact_path')

            search_button = Button(self.master, text='search',
                                   command=set_directory)
            self.exact_path_list.append((member_entry, search_button))
            search_button.place(x=self.path_x, y=len(self.exact_path_list) * 35 + start_height)
            member_entry.place(x=self.path_x + 65, y=len(self.exact_path_list) * 35 + start_height)

    def defaults(self):
        if self.is_black:
            for expression in self.data.get_black_list_regex():
                self.add_member('regex', expression)
            for expression in self.data.get_black_list_contains():
                self.add_member('contains', expression)
            for expression in self.data.get_black_list_path():
                self.add_member('exact_path', expression)

        else:
            for expression in self.data.get_white_list_regex():
                self.add_member('regex', expression)
            for expression in self.data.get_white_list_contains():
                self.add_member('contains', expression)
            for expression in self.data.get_white_list_path():
                self.add_member('exact_path', expression)

            if self.data.get_whitelist_subfolder():
                self.is_whitelist_subfolders.set(1)
            else:
                self.is_whitelist_subfolders.set(0)

    def add_list(self):
        for box, button in self.exact_path_list:
            text = box.get()
            if text.strip() and not os_util.file_exists(text):
                box.config({'background': "Red"})
                self.error_lbl.config(text='not a valid path', background='red')
                return
        if self.is_black:
            self.data.set_black_list_regex([entry.get() for entry in self.regex_list if entry.get()])
            self.data.set_black_list_contains([entry.get() for entry in self.contains_list if entry.get()])
            self.data.set_black_list_path([entry[0].get() for entry in self.exact_path_list if entry[0].get()])

        else:
            self.data.set_white_list_regex([entry.get() for entry in self.regex_list if entry.get()])
            self.data.set_white_list_contains([entry.get() for entry in self.contains_list if entry.get()])
            self.data.set_white_list_path([entry[0].get() for entry in self.exact_path_list if entry[0].get()])

            self.data.set_whitelist_subfolder(
                (lambda subfolder: True if subfolder.get() else False)(self.is_whitelist_subfolders))

        self.master.withdraw()
