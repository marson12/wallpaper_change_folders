import os_util


def get_media_list(root_path, folder_name):
    # the image files
    media_paths = []

    directory_items = os_util.get_directory_items(root_path)

    for folder in directory_items:
        full_path = os_util.join_path(root_path, folder)
        if os_util.is_folder(full_path):
            image_sub_folders = os_util.get_directory_items(full_path)

            for sub_folder in image_sub_folders:
                if folder_name in sub_folder:
                    sub_folderful_path = os_util.join_path(full_path, sub_folder)
                    media_items = os_util.get_directory_items(sub_folderful_path)

                    for media_item in media_items:
                        media_full_path = os_util.join_path(sub_folderful_path, media_item)
                        media_paths.append(media_full_path)
    return media_paths


def copy_images(media_paths, destination_path):
    for media_path in media_paths:
        os_util.copy_file(media_path, destination_path)


def move(folder_name):
    root_path = 'C:\\Users\\mason\\Pictures'

    all_path = 'C:\\Users\\mason\\Pictures\\all'
    if not os_util.file_exists(all_path):
        os_util.make_folder(all_path)

    destination_path = all_path + '\\' + folder_name
    if not os_util.file_exists(destination_path):
        os_util.make_folder(destination_path)

    media_paths = get_media_list(root_path, folder_name)
    copy_images(media_paths, destination_path)


def move_all():
    move_list = ['other', 'nsfw', 'n wallpaper']
    for folder_name in move_list:
        move(folder_name)


if __name__ == '__main__':
    move_all()
