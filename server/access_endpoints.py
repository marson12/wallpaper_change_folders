import requests
from tkinter import messagebox


def _empty_patch_request(port_number, endpoint):
    url = 'http://localhost:' + str(port_number) + endpoint
    try:
        response = requests.patch(url)
        if response.status_code != 200:
            messagebox.showinfo('status code: ' + str(response.status_code) + 'failed to send request')
    except ConnectionError as e:
        messagebox.showinfo("request error " + url, str(e))


def send_open_installer(port_number):
    _empty_patch_request(port_number, '/installer')


# endpoints
def engine_start(port_number):
    _empty_patch_request(port_number, '/start')


def engine_stop(port_number):
    _empty_patch_request(port_number, '/stop')


# sets the wallpaper to the previous image, if it was set by this engine
def set_previous_image(port_number):
    _empty_patch_request(port_number, '/previous')


# go to the next image
def skip_image(port_number):
    _empty_patch_request(port_number, '/skip')


# copies the image to a favorites file
def favorite_image(port_number):
    _empty_patch_request(port_number, '/favorite')


# moves the image to a toDelete folder
def delete_image(port_number):
    _empty_patch_request(port_number, '/delete')
