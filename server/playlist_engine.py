import os_util
from property_data import PropertyData
from property_controller import PropertyController

import time
from tkinter import messagebox


def start_engine(data, manager):
    try:
        while data.is_run_engine():
            os_util.set_wallpaper(manager.get_image())
            time.sleep(data.get_change_speed())
    except Exception as e:
        messagebox.showinfo("ERROR", str(e))
