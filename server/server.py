from tkinter import messagebox

import os_util
from server import playlist_engine
from installer_wallpaper import Installer
from server.engine_request_handler import EngineRequestHandler
from property_data import PropertyData

import multiprocessing
from http.server import HTTPServer, BaseHTTPRequestHandler
from functools import partial
import time
import os


class WallpeperServer(HTTPServer):
    def __init__(self, data, manager):
        self.data = data
        self.manager = manager
        self.engine = None
        partial_handler = partial(EngineRequestHandler, self)
        super(WallpeperServer, self).__init__(('localhost', self.data.get_port()), partial_handler)

        self.engine_start()

    def open_installer(self):
        self.engine_stop()
        Installer(self.manager)

        if not os_util.file_exists(self.manager.property_file_destination):
            os._exit(1)
        self.data.set_is_run_engine(True)
        self.engine_start()

    def engine_stop(self):
        if self.engine:
            self.engine.terminate()
            self.engine = None

    def engine_start(self):
        if not self.engine:
            self.engine = multiprocessing.Process(target=playlist_engine.start_engine,
                                                  args=(self.data, self.manager))
            self.engine.start()


    def set_previous_image(self):
        '''
        goes back to a previous image
        :return:
        '''
        self.engine_stop()
        self.manager.previous_image_to_next()
        self.engine_start()
    # moves to the next image
    def skip_image(self):
        self.engine_stop()
        self.engine_start()

    def favorite_image(self):
        self.manager.favorite_current_image()

    def delete_image(self):
        self.manager.delete_current_image()
        self.skip_image()
