from http.server import BaseHTTPRequestHandler


class EngineRequestHandler(BaseHTTPRequestHandler):
    def __init__(self, instructions_interface, *args, **kwargs):
        self.instructions = instructions_interface
        super().__init__(*args, **kwargs)

    def do_PATCH(self):
        self.send_response(200)
        self.end_headers()
        if self.path == '/installer':
            self.instructions.open_installer()
        elif self.path == '/stop':
            self.instructions.engine_stop()
        elif self.path == '/start':
            self.instructions.engine_start()
        elif self.path == '/previous':
            self.instructions.set_previous_image()
        elif self.path == '/skip':
            self.instructions.skip_image()
        elif self.path == '/favorite':
            self.instructions.favorite_image()
        elif self.path == '/delete':
            self.instructions.delete_image()
