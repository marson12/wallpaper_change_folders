from tkinter import Tk, Frame, Button, BOTH, Label, Entry, Radiobutton, IntVar, Checkbutton, messagebox
import tkinter.filedialog
from gui_classes import CreateToolTip, NumEntry, ListGui
from property_data import PropertyData
from property_controller import PropertyController
import image_convert
import os_util
from sys import exit


class Installer(Frame):
    """
    This is the main gui, and is used to collect the settings.
    the settings are turned into a data object
    then that data is sent to the properties manager.
    the property manager will then handle the installation
    """

    # Define settings upon initialization.
    def __init__(self, properties_manager):
        self.properties_manager = properties_manager
        self.data = self.properties_manager.get_data()

        self.master = Tk()
        self.master.geometry("500x600")
        self.master.title('wallpaper playlist')
        # self.master.iconbitmap('recources/icon.ico')

        Frame.__init__(self, self.master)

        self.init_window()
        self.set_defaults()

        self.master.mainloop()

    # # Creation of init_window
    def init_window(self):
        self.pack(fill=BOTH, expand=1)

        # iter to describe how far apart in the y direction each row will be for this form
        y_focus = 75
        self.x_default_form_elm = 175
        self.x_start = 50

        # message label
        self.intro_lbl = Label(self, text=
        '''This program is will change the wallpaper automatically.
        fill out the form, press the install button, forgetabout it.
        Reopening this will use properties from properties_wallpaper.ini
        mouse over labels to see more information.''')
        self.intro_lbl.place(x=self.x_start, y=0)

        # directory fields
        top_directory_lbl = Label(self, text='Top directory:')
        top_directory_lbl = CreateToolTip(top_directory_lbl,
                                          '''All subdirectories and files of the selected folder will be scanned.
                                          Then all the images will be put into the file directories_list.txt.

                                          Images directly in this folder are added to the list, while images in 
                                          subfolders may or may not be depending on the blacklist and whitelist options.
                                          If you do not want those images included then place them into a folder. 
                                          
                                          Use the search button to search through your directory easily.''')
        self.top_directory_entry = Entry(self, width=60)
        top_directory_explorer = Button(self, text='Search',
                                        command=lambda: self.set_directory(self.top_directory_entry))
        top_directory_explorer = CreateToolTip(top_directory_explorer, 'Use me to search through the directory.')

        top_directory_lbl.place(x=self.x_start, y=y_focus)
        top_directory_explorer.place(x=self.x_start + 80, y=y_focus - 5)
        y_focus += 25
        self.top_directory_entry.place(x=self.x_start, y=y_focus)

        # change times
        change_speed_lbl = Label(self, text='Your wallpaper will change once every')
        change_speed_lbl = CreateToolTip(change_speed_lbl,
                                         '''You can enter in any number into these fields and it will figure it out, 
                                         but when reopened it will set the times to the max before adding the next 
                                         value. ex: input days = 0, hours= 49. when reopened: days = 2, hours = 1. '''
                                         )
        day_lbl = Label(self, text='Day(s)')
        hour_lbl = Label(self, text='Hour(s)')
        minute_lbl = Label(self, text='Minute(s)')
        second_lbl = Label(self, text='Second(s)')

        self.day_entry = NumEntry(self.master, width=3)
        self.day_entry.bind("<Leave>", lambda x: self.check_empty_number(self.day_entry))
        self.hour_entry = NumEntry(self.master, width=3)
        self.hour_entry.bind("<Leave>", lambda x: self.check_empty_number(self.hour_entry))
        self.minute_entry = NumEntry(self.master, width=3)
        self.minute_entry.bind("<Leave>", lambda x: self.check_empty_number(self.minute_entry))
        self.second_entry = NumEntry(self.master, width=3)
        self.second_entry.bind("<Leave>", lambda x: self.check_empty_number(self.second_entry))

        y_focus += 50
        change_speed_lbl.place(x=self.x_start, y=y_focus)
        y_focus += 25
        self.day_entry.place(x=self.x_start, y=y_focus)
        day_lbl.place(x=self.x_start + 25, y=y_focus)
        self.hour_entry.place(x=self.x_start + 75, y=y_focus)
        hour_lbl.place(x=self.x_start + 100, y=y_focus)
        self.minute_entry.place(x=self.x_start + 155, y=y_focus)
        minute_lbl.place(x=self.x_start + 180, y=y_focus)
        self.second_entry.place(x=self.x_start + 250, y=y_focus)
        second_lbl.place(x=self.x_start + 275, y=y_focus)

        # image selection style
        image_type_lbl = Label(self, text='How would you like your image chosen?')
        image_type_lbl = CreateToolTip(image_type_lbl,
                                       '''
                                       Random shuffle: puts all the files into a list, randomizes the list, then
                                          whenever an image is chosen remeove that image from the list.
                                       Order: puts all the files into a list, then
                                          whenever an image is chosen remove that image from the list.
                                       Random: puts all the files into a list, randomizes the list, then
                                          picks a file and keeps it in the list.
                                          SCANS the computer on each startup.
                                       '''
                                       )
        self.image_select_var = IntVar()

        order_radio = Radiobutton(self.master,
                                  text="Order",
                                  variable=self.image_select_var,
                                  value=1)
        random_shuffle_radio = Radiobutton(self.master,
                                           text="Random Shuffle",
                                           variable=self.image_select_var,
                                           value=2)
        random_radio = Radiobutton(self.master,
                                   text="Random",
                                   variable=self.image_select_var,
                                   value=3)

        y_focus += 50
        image_type_lbl.place(x=self.x_start, y=y_focus)
        y_focus += 25
        order_radio.place(x=self.x_start, y=y_focus)
        random_shuffle_radio.place(x=self.x_start + 75, y=y_focus)
        random_radio.place(x=self.x_start + 200, y=y_focus)

        # windows alert
        self.notifier_var = IntVar()

        notifier_check = Checkbutton(self, text='Windows alert when the wallpaper changes.', variable=self.notifier_var)

        # TODO fix this notifier
        # y_focus += 50
        # notifier_check.place(x=self.x_start, y=y_focus)

        # black list button
        black_list_lbl = Label(self,
                               text='Click this to open a GUI to set which folders you want this program to exclude.')
        black_list_button = Button(self, text='Set black list',
                                   command=lambda: ListGui(self.properties_manager.get_data(), True))

        white_list_lbl = Label(self,
                               text='Click this to open a GUI to specify that you ONLY want images from these folders.')

        white_list_button = Button(self, text='Set white list',
                                   command=lambda: ListGui(self.properties_manager.get_data(), False))

        y_focus += 50
        black_list_lbl.place(x=self.x_start, y=y_focus)
        y_focus += 25
        black_list_button.place(x=self.x_default_form_elm, y=y_focus)
        y_focus += 50
        white_list_lbl.place(x=self.x_start, y=y_focus)
        y_focus += 25
        white_list_button.place(x=self.x_default_form_elm, y=y_focus)

        # install button
        self.install_button = Button(self, text="Install", command=self.install)
        self.install_button = CreateToolTip(self.install_button, '''
        MUST BE RUN IN ADMINISTRATOR MODE
        
        Adds this program to the startup so that you dont have to open this manually again.
        Puts the options into the property file properties_wallpaper.ini to be read from.
        EXIT the GUI
        Start the wallpaper_fun :D
        ''')
        y_focus += 50
        self.install_button.place(self.x_default_form_elm - 50, y_focus)

        # update button
        self.update_button = Button(self, text="Update", command=self.update)
        self.update_button = CreateToolTip(self.update_button, '''
        updates the settings. 
        ''')
        self.update_button.place(self.x_default_form_elm, y_focus)

        # uninstall button
        self.uninstall_button = Button(self, text="Uninstall", command=self.uninstall)
        self.uninstall_button = CreateToolTip(self.uninstall_button,
                                              '''MUST BE RUN IN ADMINISTRATOR MODE
                                              
                                              This button will uninstall this wallpaper program from your 
                                              computer. If you dont use this button to uninstall the computer will 
                                              keep trying to open this exe file every time the computer turns on. 
                                              Delete the property files and file lists that the installer created. 
                                              Stop the program from automatically starting.''')
        self.uninstall_button.place(x=self.x_default_form_elm + 55, y=y_focus)

        convert_directory_lbl = Label(self, text='convert images:')
        convert_directory_lbl = CreateToolTip(convert_directory_lbl,
                                              '''This is for converting image files. 
                                              Some image files cannot be used as a desktop background (webp, ...)
                                              and thus a conversion can be useful, especially while you are already
                                              using this program.''')
        self.convert_directory_entry = Entry(self, width=60)

        directory_explorer = Button(self, text='Search',
                                    command=lambda: self.set_directory(self.convert_directory_entry))
        directory_explorer = CreateToolTip(directory_explorer,
                                           'Use me to find the directory that needs to be converted.')

        convert_button = Button(self, text='Convert',
                                command=self.convert_folder)
        convert_button = CreateToolTip(convert_button,
                                       '''Converts image files that cannot be used as a desktop background
                                       I will scan all the files in this folder, including subfolders . I will 
                                       create the folders "CONVERTED" and "BAD_FILE_TYPE" inside each folder or 
                                       subfolder that has a file to be converted. For each file I scaned and 
                                       evaluated to be converted I will create a png version and put it into 
                                       CONVERTED put that file into BAD_FILE_TYPE''')
        # Engine Off Button
        y_focus += 50
        self.engine_stop = Button(self, text="Playlist Off", command=self.exit)
        self.engine_stop = CreateToolTip(self.engine_stop, '''
        Turn off the engine.
        ''')
        self.engine_stop.place(self.x_default_form_elm, y_focus)

        # convert image files to
        y_focus += 50
        convert_directory_lbl.place(x=self.x_start, y=y_focus)
        directory_explorer.place(x=self.x_start + 90, y=y_focus - 5)
        convert_button.place(x=self.x_start + 145, y=y_focus - 5)
        y_focus += 25
        self.convert_directory_entry.place(x=self.x_start, y=y_focus)

    def set_defaults(self):
        data = self.properties_manager.get_data()

        # set directory
        self.top_directory_entry.insert(0, data.get_top_directory())

        # set change time
        time = data.get_change_speed()

        days = int(time / (60 * 60 * 24))
        self.day_entry.insert(0, days)
        time -= days * 60 * 60 * 24

        hours = int(time / (60 * 60))
        self.hour_entry.insert(0, hours)
        time -= hours * 60 * 60

        minutes = int(time / 60)
        self.minute_entry.insert(0, minutes)
        time -= minutes * 60

        seconds = time
        self.second_entry.insert(0, seconds)

        # set image selection style
        if data.get_random_style() == 'order':
            self.image_select_var.set(1)
        elif data.get_random_style() == 'random shuffle':
            self.image_select_var.set(2)
        elif data.get_random_style() == 'random':
            self.image_select_var.set(3)

        # notifier boolean
        if data.get_notifier_boolean():
            self.notifier_var.set(1)
        else:
            self.notifier_var.set(0)

        self.data.set_is_run_engine(False)

    def uninstall(self):
        is_quit = messagebox.askquestion('Exit Application', 'Are you sure you want to uninstall',
                                         icon='warning')
        if is_quit:
            error = self.properties_manager.uninstall()
            if error:
                self.error_message(error)
            else:
                self.intro_lbl.configure(text="uninstalled", bg='green')
                self.intro_lbl.place(x=self.x_default_form_elm)
                self.master.destroy()

    def update(self):
        self.install(True)

    def install(self, isUpdate=False):
        self.install_button.close()

        self.data.set_is_run_engine(True)

        if self.image_select_var.get() == 1:
            self.data.set_random_style('order')
        elif self.image_select_var.get() == 2:
            self.data.set_random_style('random shuffle')
        elif self.image_select_var.get() == 3:
            self.data.set_random_style('random')

        directory = self.top_directory_entry.get()
        if not os_util.file_exists(directory):
            self.error_message('the top directory does not exist')
            return

        self.data.set_top_directory(directory)

        # time property
        time = self.entry_to_float(self.day_entry) * 24 + self.entry_to_float(self.hour_entry)
        time = time * 60 + self.entry_to_float(self.minute_entry)
        time = time * 60 + self.entry_to_float(self.second_entry)

        if not time:
            self.error_message('total time cannot be 0')
            return
        self.data.set_change_speed(time)

        if self.image_select_var.get() == 1:
            self.data.set_random_style('order')
        elif self.image_select_var.get() == 2:
            self.data.set_random_style('random shuffle')
        elif self.image_select_var.get() == 3:
            self.data.set_random_style('random')

        if self.notifier_var.get():
            # TODO fix notifier
            # data.set_notifier_boolean(True)
            self.data.set_notifier_boolean(False)
        else:
            self.data.set_notifier_boolean(False)

        if isUpdate:
            return_msg = self.properties_manager.update()
        else:
            return_msg = self.properties_manager.install()

        if return_msg:
            self.error_message(return_msg)
            return

        self.master.destroy()

    def exit(self):
        exit()

    def entry_to_float(self, entry):
        if entry.get().strip():
            return float(entry.get())
        else:
            return 0

    def set_directory(self, entry):
        folder_path = tkinter.filedialog.askdirectory()
        # doesnt set '' to the field
        if os_util.is_folder(folder_path):
            entry.delete(0, tkinter.END)
            entry.insert(0, folder_path)

    def convert_folder(self):
        folder_path = self.convert_directory_entry.get()

        if os_util.is_folder(folder_path):
            image_convert.convert_folder(folder_path)

    def error_message(self, message):
        self.intro_lbl.configure(text=message, bg='red')
        self.intro_lbl.place(x=self.x_default_form_elm)

    def check_empty_number(self, entry):
        if not entry.get():
            entry.insert(0, 0)
